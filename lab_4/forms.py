from django import forms
from django.forms import widgets
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        widgets = {
            'untuk': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter the receiver'}),
            'dari': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter the sender'}),
            'judul': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Enter the title'}),
            'pesan': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Your message...'})
        }