Apakah perbedaan antara JSON dan XML?
* JSON merupakan sebuah format data, sedangkan XML adalah sebuah bahasa markup.
* JSON memiliki syntax yang lebih sederhana dibandingkan dengan XML.
* Data pada JSON disimpan dalam bentuk key-value pair, sedangkan pada XML data disimpan di dalam sebuah tag yang bersesuaian.

Apakah perbedaan antara HTML dan XML?
* HTML adalah bahasa markup yang digunakan untuk menampilkan data, sedangkan XML adalah bahasa markup yang digunakan untuk
mentransfer dan menyimpan data.
* Pada HTML kita harus menggunakan predefined tag yang sudah ditentukan oleh pencipta bahasa, sedangkan pada XML tidak terdapat predefined tag sehingga kita harus membuat tag sendiri sesuai dengan kebutuhan kita.
* HTML bersifat statis karena hanya menampilkan data, sedangkan XML bersifat dinamis karena digunakan untuk mentransfer data.
