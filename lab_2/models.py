from django.db import models

# Create your models here.
class Note(models.Model):
    untuk = models.CharField(verbose_name="To", max_length=30, null = True, blank=True)
    dari = models.CharField(verbose_name="From",max_length=30, null = True, blank=True)
    judul = models.CharField(verbose_name="Title", max_length=30, null = True, blank=True)
    pesan = models.CharField(verbose_name="Message",max_length=200, null = True, blank=True)
    
