import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "Jurnal kesehatan mental untuk kehidupan yang lebih sehat di masa pandemi.",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Kesehatan mental menjadi hal krusial untuk diperhatikan di tengah pandemi COVID-19 yang tak kunjung membaik ini. Kami yakin bahwa hidup terasa semakin sulit dari hari ke hari setelah lebih dari satu tahun hidup di bawah ancaman virus corona dengan interaksi sosial yang terbatas. Dengan adanya keterbatasan ini, semakin banyak orang yang mengalami depresi dan frustasi karena mereka tidak lagi memiliki tempat untuk mencurahkan isi hatinya akibat canggung untuk berinteraksi dengan orang lain. Oleh karena itu, kami membuat aplikasi situs web bernama reflekt.io yang dapat menjadi tempat aman baru bagi semua orang. Aplikasi situs web ini memiliki berbagai fitur dapat membantu menjaga kesehatan mental kita di situasi sulit seperti ini. Kami berharap aplikasi situs web ini dapat membantu orang-orang untuk menjalani hidup dengan lebih mudah di masa pandemi ini.",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                SizedBox(
                  height: 15,
                ),
                ElevatedButton.icon(
                    onPressed: () {},
                    icon: Icon(Icons.mail),
                    label: Text("Contact Us"))
              ],
            )
          ],
        ),
      ),
    );
  }
}
